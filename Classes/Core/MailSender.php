<?php

namespace App;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class MailSender
{
    public function __construct()
    {
        $this->mail = new PHPMailer(true);
        try {
            $this->mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $this->mail->isSMTP();
            $this->mail->Host = MAIL_AUTHORIZATION['HOST'];
            $this->mail->SMTPAuth = MAIL_AUTHORIZATION['SMTPAUTH'];
            $this->mail->Username = MAIL_AUTHORIZATION['USER'];
            $this->mail->Password = MAIL_AUTHORIZATION['PASSWORD'];
            $this->mail->SMTPSecure = MAIL_AUTHORIZATION['SMTPSECURE'];
            $this->mail->Port = MAIL_AUTHORIZATION['PORT'];

            $this->mail->setFrom(MAIL_AUTHORIZATION['SENT_FROM'], MAIL_AUTHORIZATION['YOUR_NAME']);

            $this->mail->addReplyTo(MAIL_AUTHORIZATION['REPLY_TO'], MAIL_AUTHORIZATION['YOUR_NAME']);

            $this->mail->isHTML(true);
        } catch (Exception $e) {
        }
    }

    public function addAddress(string $mail)
    {
        $this->mail->addAddress($mail);
        return $this;
    }

    public function addSubject(string $subject)
    {
        $this->mail->Subject = $subject;
        return $this;
    }

    public function addBody(string $body)
    {
        $this->mail->Body = $body;
        return $this;
    }

    public function addAltBody(string $altBody)
    {
        $this->mail->AltBody = $altBody;
        return $this;
    }

    public function send()
    {
        return $this->mail->send();
    }
}
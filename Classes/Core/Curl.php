<?php

namespace App;

class Curl
{
    private $curl;

    public function __construct()
    {
        $this->curl = \curl_init();
        $user_agent = 'Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';
        \curl_setopt($this->curl, CURLOPT_USERAGENT, $user_agent);
        \curl_setopt($this->curl, CURLOPT_HEADER, 0);
        \curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, 1);
        \curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        \curl_setopt($this->curl, CURLOPT_CONNECTTIMEOUT, 120);
        \curl_setopt($this->curl, CURLOPT_TIMEOUT, 120);
        \curl_setopt($this->curl, CURLOPT_MAXREDIRS, 10);
        \curl_setopt($this->curl, CURLOPT_COOKIEFILE, "cookie.txt");
        \curl_setopt($this->curl, CURLOPT_COOKIEJAR, "cookie.txt");
    }

    public function __destruct()
    {
        \curl_close($this->curl);
    }

    public function get($url)
    {
        \curl_setopt($this->curl, CURLOPT_URL, $url);
        return \curl_exec($this->curl);
    }

    public function post($url, $data)
    {
        \curl_setopt($this->curl, CURLOPT_URL, $url);
        \curl_setopt($this->curl, CURLOPT_POST, true);
        \curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
        \curl_setopt($this->curl, CURLOPT_HTTPHEADER,     array('Content-Type: application/json'));
        return \curl_exec($this->curl);
    }
}
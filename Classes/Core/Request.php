<?php

namespace App;


use App\Database\DB;

class Request
{
    use SetDataTrait;

    private $data;
    private $ipId;
    private $userId;
    private $user;

    public function __construct(int $userId = null)
    {
        $this->userId = $userId;
        $this->data = [];
        if (isset($_POST)) {
            if (count($_POST) == 0)
                $_POST = json_decode(file_get_contents('php://input'), true);
            foreach ((array)$_POST as $key => $value) {
                $this->data[$key] = $value;
            }
            unset($_POST);
        }
        if (isset($_GET)) {
            foreach ($_GET as $key => $value) {
                $this->data[$key] = $value;
            }
            unset($_GET);
        }
    }

    public function addData($datas)
    {
        foreach ($datas as $key => $data) {
            $this->data[$key] = $data;
        }
    }

    public function initDataSet()
    {
        $this->setData($this->data);
    }

    public function getCurrentUserId()
    {
        return $this->userId;
    }

    public function getCurrentIpId()
    {
        return $this->ipId;
    }


    public function setupIpId(int $sessionId = null)
    {
        $ip = null;
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        $hostName = gethostbyaddr($ip);

        $ip = ip2long($ip) ? ip2long($ip) : 1;
        $this->ipId = DB::get()->getOne('select id from ips where ip=:ip and session_id=:session_id', [
            'ip' => $ip,
            'session_id' => $sessionId,
        ]);
        if (isset($this->ipId)) {
            DB::get()->execute('update ips set `date`=:date where id=:id', [
                'id' => $this->ipId,
                'date' => time(),
            ]);
        } else {
            $curl = new Curl;
            $informations = $curl->get('http://api.ipstack.com/'.$ip.'?access_key=2880af1ab213fce533521e4e471e827b&output=json&legacy=1');
            $informations = json_decode($informations);

            DB::get()->execute('insert into ips (ip, user_id, `date`, session_id, host_name, country_name, region_name, 
                city, zip_code, latitude, longitude) 
                values (:ip, :user_id, :date, :session_id, :host_name, :country_name, :region_name, :city, :zip_code, 
                :latitude, :longitude)', [
                'ip' => $ip,
                'user_id' => $this->userId,
                'date' => time(),
                'session_id' => $sessionId,
                'host_name' => $hostName,
                'country_name' => $informations->country_name,
                'region_name' => $informations->region_name,
                'city' => $informations->city,
                'zip_code' => $informations->zip_code,
                'latitude' => $informations->latitude,
                'longitude' => $informations->longitude,
            ]);
            $this->ipId = DB::get()->insertId();
        }
        $_SESSION['ip_id'] = $this->ipId;
    }

    public function getIpId()
    {
        return $this->ipId;
    }

    public function setUserId(int $userId = null)
    {
        if (!isset($userId)) {
            return;
        }
        $this->userId = $userId;
        $this->user = new User($userId);
    }

    public function getUser(): ?User
    {
        if (!isset($this->user) && isset($this->userId)) {
            $this->user = new User($this->userId);
        }
        return $this->user;
    }
}

<?php

namespace App;

use App\Database\DB;
use App\Types\SortKind;

class Model
{
    use SetDataTrait;

    public $setDefaultFields = true;
    private $tableName;
    private $fields;
    private $db;
    private $where;
    private $loaded;
    private $sortName;
    private $sortKind;
    private $filters;
    private $totalCount;
    private $loading;
    private $userId;
    private $ipId;

    public function __construct($data = null)
    {
        if ($data) {
            $this->setData($data);
        }
        if(!$this->userId){
            $this->userId = isset($_SESSION['user_id'])?$_SESSION['user_id']:null;
        }
        if(!$this->ipId){
            $this->ipId = $_SESSION['ip_id'];
        }
        $reflectionClass = new \ReflectionClass($this);
        $className = $reflectionClass->getName();
        $names = explode('\\', $className);
        $name = $names[count($names) - 1];
        if (strpos($name, 'Model')) {
            $name = substr($name, 0, -strlen('Model'));
        }
        if (strpos($name, 'Collection')) {
            $name = substr($name, 0, -strlen('Collection'));
        }
        $this->tableName = $this->getFromCamelCase($name);

        $this->db = DB::get();

        $this->loading = false;
        $this->fields = [];
    }

    public function startTransaction()
    {
        $this->db->execute('start transaction');
    }

    public function commit()
    {
        $this->db->execute('commit');
    }

    public function rollback()
    {
        $this->db->execute('rollback');
    }

    public function getFromCamelCase($name)
    {
        $tableName = lcfirst($name);
        $tableName = preg_replace_callback('/[A-Z]/', function ($match) {
            return '_' . lcfirst($match[0]);
        }, $tableName);
        return $tableName;
    }

    public function setFields()
    {
        $this->fields = [];
        $reflectionClass = new \ReflectionClass($this);
        $methods = $reflectionClass->getMethods();
        foreach ($methods as $method) {
            if (strpos($method->getName(), 'set') === 0) {
                $getter = 'get' . substr($method->getName(), 3);
                if (method_exists($this, $getter) && !in_array(lcfirst(substr($method->getName(), 3)), ['limit', 'page', 'totalCount'])) {
                    $this->fields[$this->getFromCamelCase(substr($method->getName(), 3))] = null;
                }
            }
        }
    }

    public function insert(): int
    {
        $fields = [];
        $params = [];
        if ($this->setDefaultFields) {
            $fields[] = ' `added`=? ';
            $fields[] = ' `added_by`=? ';
            $fields[] = ' `added_ip_id`=? ';
            $params[] = time();
            $params[] = $this->userId;
            $params[] = $this->ipId;
        }
        foreach ($this->fields as $name => $value) {
            $fields[] = ' `' . $name . '`=? ';
            $params[] = $value;
        }
        $fieldsSQL = join(', ', $fields);

        $this->db->execute('insert into `' . $this->tableName . '` set ' . $fieldsSQL, $params);

        if ($this->setDefaultFields&&method_exists($this, 'setId')) {
            $this->setId($this->db->insertId());
        }

        return $this->db->insertId();
    }

    public function where($sql, $param)
    {
        if (!isset($param)) {
            return $this;
        }
        $this->where[$sql] = $param;
        return $this;
    }

    public function load($id = null, bool $uuid = false)
    {
        $this->setFields();

        $fields = [];
        foreach ($this->fields as $name => $value) {
            $fields[] = ' `' . $name . '` ';
        }
        $fieldsSQL = join(', ', $fields);
        if(count($fields)==0){
            $fieldsSQL = '*';
        }

        $where = [];
        if ($this->setDefaultFields) {
            $where[] = ' `deleted`=0 ';
        }
        $params = [];
        if ($this->where) {
            foreach ($this->where as $sql => $param) {
                if (strpos($sql, 'uuid')) {
                    $where[] = $sql;
                    $params[] = hex2bin((string)$param);
                } else {
                    $where[] = $sql;
                    if(is_array($param)) {
                        foreach($param as $par) {
                            $params[] = $par;
                        }
                    }else{
                        $params[] = $param;
                    }
                }
            }
        }
        $whereSQL = join(' and ', $where);
        if (strlen($whereSQL)) {
            $whereSQL = ' where ' . $whereSQL;
        }

        $row = [];
        if ($id) {
            $whereSQL = (($whereSQL) ? $whereSQL . ' and ' : 'where') . ($uuid ? ' `uuid`=? ' : ' `id`=? ');
            $params[] = $uuid ? hex2bin((string)$id) : $id;
        }

        if (($this->where && count($this->where) > 0) || $id) {
            $row = $this->db->getRow('select ' . $fieldsSQL . ' from `' . $this->tableName . '` ' . $whereSQL . ' limit 1', $params);
        }

        $data = [];
        if (isset($row) && is_array($row)) {
            foreach ($row as $key => $value) {
                $newKey = preg_replace_callback('/(_[a-z])/', function ($match) {
                    return ucfirst(substr($match[0], 1));
                }, $key);
                $data[$newKey] = $value;
            }
        }

        $this->loading = true;
        $this->fields = [];
        if(isset($data['id'])) {
            $this->fields['id'] = $data['id'];
        }
        $this->setData($data);
        $this->loading = false;

        $this->loaded = $row && (count($row) > 0);

        return $this;
    }

    public function isLoaded()
    {
        return $this->loaded ? true : false;
    }

    public function loadAll()
    {
        $this->setFields();

        $where = [];

        $fields = [];
        foreach ($this->fields as $name => $value) {
            $fields[] = ' `' . $name . '` ';
        }
        $fieldsSQL = join(', ', $fields);
        if(count($fields)==0){
            $fieldsSQL = '*';
        }

        if ($this->setDefaultFields) {
            $where[] = ' `deleted`=0 ';
        }
        $params = [];
        if ($this->where) {
            foreach ($this->where as $sql => $param) {
                $where[] = $sql;
                if(is_array($param)) {
                    foreach($param as $par) {
                        $params[] = $par;
                    }
                }else{
                    $params[] = $param;
                }
            }
        }
        if ($this->filters) {
            foreach ($this->filters as $filter) {
                $name = $filter->getName();
                $kind = (string)$filter->getKind();
                if (in_array('set' . ucfirst($name), get_class_methods($this))) {
                    $name = $this->getFromCamelCase($name);
                    if($kind=='%') {
                        $where[] = ' `' . $name . '` like concat(\'%\', ?, \'%\') ';
                    }else if($kind=='=') {
                        $where[] = ' `' . $name . '` =? ';
                    }else if($kind=='!=') {
                        $where[] = ' `' . $name . '` <>? ';
                    }else if($kind=='<>') {
                        $where[] = ' `' . $name . '` <>? ';
                    }else if($kind=='>') {
                        $where[] = ' `' . $name . '` >? ';
                    }else if($kind=='<') {
                        $where[] = ' `' . $name . '` <? ';
                    }else if($kind=='null') {
                        $where[] = ' `' . $name . '` is null ';
                    }else if($kind=='not null') {
                        $where[] = ' `' . $name . '` is not null ';
                    }
                    if(($kind!='null')&&($kind!='not null')) {
                        $value = $filter->getValue();
                        if($value===true){
                            $value = 1;
                        }else if($value===false){
                            $value = 0;
                        }
                        $params[] = $value;
                    }
                }
            }
        }
        $whereSQL = '';
        if(count($where)>0) {
            $whereSQL = join(' and ', $where);
            if (strlen($whereSQL)) {
                $whereSQL = ' where ' . $whereSQL;
            }
        }

        $sortSQL = '';
        if ($this->sortKind) {
            $setter = ucfirst(preg_replace_callback('/(_[a-z])/', function($match){
                return strtoupper($match[1][1]);
            }, $this->sortName));
            if (in_array('set' . $setter, get_class_methods($this))) {
                $name = $this->getFromCamelCase($this->sortName);
                $sortKind = (string)$this->sortKind;
                if($sortKind=='rand'){
                    $sortKind='rand()';
                }
                $sortSQL = ' order by '.($name?('`' . $name . '` '):'').' '.$sortKind . ' ';
            }
        }

        if (empty($sortSQL)&&$this->setDefaultFields) {
            $sortSQL = ' order by id desc ';
        }

        $limit = '';
        if (method_exists($this, 'getPage') && $this->getLimit()) {
            $page = $this->getPage();
            if(!$page){
                $page = 1;
            }
            $limit = ' limit ' . (($page - 1) * $this->getLimit()) . ',' . $this->getLimit();
        }

        $totalCount = $this->db->getOne('select coalesce(count(*),0) from `' . $this->tableName . '` ' . $whereSQL, $params);
        $this->setTotalCount($totalCount);
        $sql = 'select ' . $fieldsSQL . ' from `' . $this->tableName . '` ' . $whereSQL . $sortSQL . $limit;
        $rows = $this->db->getAll($sql, $params);

        $data = [];
        if (is_array($rows)) {
            foreach ($rows as $rowKey => $rowValue) {
                foreach ($rowValue as $key => $value) {
                    $newKey = preg_replace_callback('/(_[a-z])/', function ($match) {
                        return ucfirst(substr($match[0], 1));
                    }, $key);
                    $data[$rowKey][$newKey] = $value;
                }
            }
        }
        $this->loading = true;
        $this->fields = [];
        $this->setData($data);
        $this->loading = false;

        $this->loaded = $rows ? true : false;

        return $this;
    }

    private function setTotalCount(int $totalCount = 0)
    {
        $this->totalCount = $totalCount;
    }

    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    public function update($id = null, bool $uuid = false)
    {
        $whereSQL = '';
        $fields = [];
        $params = [];
        if ($this->setDefaultFields) {
            $fields[] = ' `updated`=? ';
            $fields[] = ' `updated_by`=? ';
            $fields[] = ' `updated_ip_id`=? ';
            $params[] = time();
            $params[] = $this->userId;
            $params[] = $this->ipId;
        }
        foreach ($this->fields as $name => $value) {
            if (($name == 'uuid') || ($name == 'id')) {
                continue;
            }
            $fields[] = ' `' . $name . '`=? ';
            $params[] = $value;
        }
        $fieldsSQL = join(', ', $fields);

        if (isset($id)) {
            if ($uuid) {
                $whereSQL = ' `uuid`=? ';
                $params[] = hex2bin($id);
            } else {
                $whereSQL = ' `id`=? ';
                $params[] = $id;
            }
        } else if (isset($this->fields['uuid'])) {
            $whereSQL = ' `uuid`=? ';
            $params[] = $this->fields['uuid'];
        } else if (isset($this->fields['id'])) {
            $whereSQL = ' `id`=? ';
            $params[] = $this->fields['id'];
        }
        if(!empty($whereSQL)){
            $whereSQL = ' where '.$whereSQL;
        }

        return $this->db->execute('update `' . $this->tableName . '` set ' . $fieldsSQL . ' ' . $whereSQL, $params) ? true : false;
    }

    public function delete($id = null, bool $uuid = false)
    {
        $whereSQL = [];
        $params = [];
        if ($this->setDefaultFields) {
            $params[] = time();
            $params[] = $this->userId;
            $params[] = $this->ipId;
        }

        if (isset($id)) {
            if ($uuid) {
                $whereSQL[] = ' `uuid`=? ';
                $params[] = hex2bin((string)$id);
            } else {
                $whereSQL[] = ' `id`=? ';
                $params[] = $id;
            }
        } else if (isset($this->fields['uuid'])) {
            $whereSQL[] = ' `uuid`=? ';
            $params[] = $this->fields['uuid'];
        } else {
            if (isset($this->fields['id'])) {
                $whereSQL[] = ' `id`=? ';
                $params[] = $this->fields['id'];
            }
        }

        $where = '';
        $whereSql = join(' and ', $whereSQL);
        if (!empty($whereSql)) {
            $where = ' where ' . $whereSql;
        }
        return $this->db->execute('update `' . $this->tableName . '` set ' . ($this->setDefaultFields ? 'deleted=?, deleted_by=?, deleted_ip_id=? ' : '') . $where, $params) ? true : false;
    }

    public function forceDelete($id = null, bool $uuid = false)
    {
        $whereSQL = [];
        $params = [];

        if (isset($id)) {
            if ($uuid) {
                $whereSQL[] = ' `uuid`=? ';
                $params[] = hex2bin((string)$id);
            } else {
                $whereSQL[] = ' `id`=? ';
                $params[] = $id;
            }
        } else if (isset($this->fields['uuid'])) {
            $whereSQL[] = ' `uuid`=? ';
            $params[] = $this->fields['uuid'];
        } else {
            if (isset($this->fields['id'])) {
                $whereSQL[] = ' `id`=? ';
                $params[] = $this->fields['id'];
            }
        }

        $where = '';
        $whereSql = join(' and ', $whereSQL);
        if (!empty($whereSql)) {
            $where = ' where ' . $whereSql;
        }
        return $this->db->execute('delete from `' . $this->tableName . '` ' . $where, $params) ? true : false;
    }

    public function set($name, $value)
    {
        if(!$this->loading) {
            $this->fields[$name] = $value;
        }
    }

    public function setSortName(string $sortName = null)
    {
        $this->sortName = $sortName;
        return $this;
    }

    public function setSortKind(SortKind $sortKind = null)
    {
        $this->sortKind = $sortKind;
        return $this;
    }

    public function setFilters($filters)
    {
        $this->filters = $filters;
        return $this;
    }
}
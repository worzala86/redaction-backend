<?php

function exceptionHandler(/*Exception */$exception)
{
    $className = get_class($exception);
    $classNameParts = explode('\\', $className);
    $exceptionName = $classNameParts[count($classNameParts) - 1];
    if (DEBUG) {
        $lines = [];
        $lines[] = $exception->getFile() . ' (' . $exception->getLine() . ')';
        $backtrace = $exception->getTrace();
        foreach ($backtrace as $key => $row) {
            if (isset($row['class'])) {
                $lines[] = $row['class'] . ' (' . $row['function'] . ')';
            } else {
                $lines[] = $row['file'] . ' (' . $row['line'] . ')';
            }
        }
        echo json_encode([
            'error' => $exceptionName,
            'message' => $exception->getMessage(),
            'lines' => $lines,
        ]);
    } else {
        echo json_encode(['error' => $exceptionName]);
    }
    exit;
}

set_exception_handler('exceptionHandler');

<?php

namespace App;

use App\Exceptions\WrongElementKindTypeException;
use App\Type;

class FiltersKind extends Type
{
    static $pattern = '(=|!=|<>|<|>|null|not null|%)';

    public function __construct($data = null)
    {
        if(!in_array($data, ['=', '!=', '<>', '<', '>', 'null', 'not null', '%'])){
            throw new WrongElementKindTypeException;
        }
        parent::__construct($data);
    }
}
<?php

namespace App;

header("Access-Control-Allow-Origin: http://localhost:4200");
header('Access-Control-Allow-Credentials: true');

if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
    header('Access-Control-Allow-Headers: Content-Type');
    header("HTTP/1.1 200 OK");
    die();
}

require_once('ExceptionHandler.php');

require_once('../../Config.php');
require_once('../../vendor/autoload.php');
require_once('Autoload.php');

$routing = new Routing;
$routing();

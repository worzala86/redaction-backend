<?php

namespace App;

use App\Exceptions\MissingRegexInRouteException;
use App\Exceptions\RequestedUrlNotFoundException;
use App\Exceptions\RoutingHandlerNotFoundException;

class Routing
{
    private $uri;
    private $routes;
    private $session;

    public function __construct()
    {
        $this->session = new Session;
        $uri = $_SERVER['REQUEST_URI'];
        if (strpos($uri, '?')) {
            $this->uri = substr($uri, 0, strpos($uri, '?'));
        } else {
            $this->uri = $uri;
        }
        $this->routes = [];
        foreach (glob(substr(__DIR__, 0, strpos(__DIR__, '/Core')) . '/Modules/*/Route.php') as $routeDir) {
            $routesFromDir = require_once($routeDir);
            foreach ($routesFromDir as $route) {
                $this->routes[] = $route;
            }
        }
    }

    public function __invoke()
    {
        $response = null;
        foreach ($this->routes as $route) {
            $urlParams = [];
            $url = $route['url'];
            $urls = explode('/', $url);
            foreach ($urls as $key => $url) {
                if ((strlen($url) > 0) && $url[0] == ':') {
                    $regexKey = substr($url, 1);
                    if (!isset($route['regex'][$regexKey])) {
                        throw new MissingRegexInRouteException;
                    }
                    $urls[$key] = (string)$route['regex'][$regexKey];
                    $urlParams[] = $regexKey;
                }
            }
            $url = '\/' . join('\/', $urls);
            if (preg_match_all('/^' . $url . '$/', $this->uri, $matches) && ($_SERVER['REQUEST_METHOD'] == strtoupper($route['method']))) {
                if (!class_exists($route['handler'])) {
                    throw new RoutingHandlerNotFoundException;
                }
                $handler = new $route['handler'];
                $reflectionMethod = (new \ReflectionClass($handler))->getMethod('__invoke');
                $parameters = $reflectionMethod->getParameters();
                $params = [];
                foreach ($parameters as $parameter) {
                    $data = [];
                    foreach ($urlParams as $key => $p) {
                        $data[$p] = $matches[$key + 1][0];
                    }
                    $requestName = $parameter->getType()->getName();
                    $request = new $requestName($this->session->getUserId());
                    $request->setupIpId($this->session->getSessionId());
                    $request->setUserId($this->session->getUserId());
                    $request->addData($data);
                    $request->initDataSet();
                    $params[] = $request;
                }
                $response = call_user_func_array($handler, $params);
                break;
            }
        }

        if (!$response) {
            throw new RequestedUrlNotFoundException;
        }

        $resultData = $response->getFlatData();
        $resultData = $this->clean($resultData);

        echo json_encode($resultData);
    }

    private function clean($data)
    {
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $data[$key] = $this->clean($value);
                if ($data[$key] === null) {
                    unset($data[$key]);
                }
            }
        } else if (is_object($data)) {
            foreach (get_class_methods($data) as $method) {
                $data->{$method} = $this->clean($data->{$method});
                if ($data->{$method} === null) {
                    unset($data->{$method});
                }
            }
        }
        return $data;
    }
}
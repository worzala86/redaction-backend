<?php

namespace App;


use App\Exceptions\MustBeLoggedToUseThisEndpointException;

class UserRequest extends Request
{
    public function __construct(int $userId = null)
    {
        parent::__construct($userId);
        if (!$this->getCurrentUserId()) {
            throw new MustBeLoggedToUseThisEndpointException;
        }
    }
}
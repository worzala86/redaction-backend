<?php

namespace App\Modules\Files\Responses;

use App\Containers\ImagesContainer;
use App\Response;

class UploadFilesResponse extends Response
{
    private $files;

    /**
     * @param ImagesContainer $files
     * @description Lista nowo dodanych plików, zawiera identyfikatory i nowe nazwy plików
     * @return $this
     */
    public function setFiles(ImagesContainer $files){
        $this->files = $files;
        return $this;
    }

    public function getFiles(): ImagesContainer {
        return $this->files;
    }
}
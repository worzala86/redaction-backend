<?php

namespace App;

use App\Modules\Files\Handlers;
use App\Types\UUID;

return [
    ['method'=>'post', 'url'=>'api/files', 'handler'=>Handlers\UploadFilesHandler::class],
    ['method'=>'get', 'url'=>'pliki/:id/:name', 'handler'=>Handlers\GetFileHandler::class, 'regex'=>['id'=>UUID::$pattern, 'name'=>'(.*)']],
];
<?php

namespace App\Modules\Files\Exceptions;

use App\NamedException;

class FileNotFoundException extends \Exception implements NamedException
{
}
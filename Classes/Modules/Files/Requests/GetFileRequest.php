<?php

namespace App\Modules\Files\Requests;

use App\Containers\FilesContainer;
use App\Types\FileTarget;
use App\Types\UUID;
use App\UserRequest;

class GetFileRequest extends UserRequest
{
    private $id;
    private $name;

    /**
     * @param UUID $id
     * @description Identyfikator pliku
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @description Nazwa pliku
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
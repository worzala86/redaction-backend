<?php

namespace App\Modules\Files\Handlers;

use App\File;
use App\Handler;
use App\Modules\Files\Exceptions\FileNotFoundException;
use App\Modules\Files\Models\FilesDownloadsModel;
use App\Modules\Files\Models\FilesModel;
use App\Modules\Files\Requests\GetFileRequest;
use App\Modules\Files\Responses\GetFileResponse;
use App\Types\UUID;

/**
 * Class GetFileHandler
 * @package App\Modules\Files\Handlers
 * @description Metoda służy do dodawania pliku
 */
class GetFileHandler extends Handler
{
    public function __invoke(GetFileRequest $request): GetFileResponse
    {
        $filesModel = (new FilesModel)
            ->where('`name`=?', $request->getName())
            ->load($request->getId(), true);
        if(!$filesModel->isLoaded()){
            throw new FileNotFoundException;
        }

        (new FilesDownloadsModel)
            ->setUuid(UUID::fake())
            ->setFileId($filesModel->getId())
            ->setUserId($request->getCurrentUserId())
            ->insert();

        $file = new File;
        $file->load($filesModel->getId());

        echo $file->getStream($filesModel->getUuid(), $filesModel->getName());
        exit;
    }
}

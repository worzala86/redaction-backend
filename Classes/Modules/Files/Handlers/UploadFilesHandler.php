<?php

namespace App\Modules\Files\Handlers;

use App\Handler;
use App\Modules\Files\Requests\UploadFilesRequest;
use App\Modules\Files\Responses\UploadFilesResponse;

/**
 * Class UploadFilesHandler
 * @package App\Modules\Files\Handlers
 * @description Metoda służy do dodawania pliku
 */
class UploadFilesHandler extends Handler
{
    public function __invoke(UploadFilesRequest $request): UploadFilesResponse
    {
        print_r($_FILES);
        exit;

        return (new UploadFilesResponse)
            ->setFiles($filesResponse);
    }
}

<?php

namespace App\Modules\User\Models;

use App\Model;
use App\Types\Mail;
use App\Types\Password;
use App\Types\Time;
use App\Types\UUID;

class UsersModel extends Model
{
    private $mail;
    private $password;
    private $id;
    private $reset;
    private $admin;
    private $uuid;
    private $added;

    public function setAdded(Time $added)
    {
        $this->set('added', $added);
        $this->added = $added;
        return $this;
    }

    public function getAdded(): Time
    {
        return $this->added;
    }

    public function setUuid(UUID $uuid)
    {
        $this->set('uuid', hex2bin($uuid));
        $this->uuid = $uuid;
        return $this;
    }

    public function getUuid(): UUID
    {
        return $this->uuid;
    }

    public function setAdmin(bool $admin)
    {
        $this->set('admin', $admin);
        $this->admin = $admin;
        return $this;
    }

    public function getAdmin(): bool
    {
        return $this->admin;
    }

    public function setReset(UUID $reset = null)
    {
        $this->set('reset', hex2bin($reset));
        $this->reset = $reset;
        return $this;
    }

    public function getReset(): ?UUID
    {
        return $this->reset;
    }

    public function setId(int $id)
    {
        $this->set('id', $id);
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setMail(Mail $mail)
    {
        $this->set('mail', $mail);
        $this->mail = $mail;
        return $this;
    }

    public function getMail(): Mail
    {
        return $this->mail;
    }

    public function setPassword(Password $password)
    {
        $this->set('password', hex2bin($password));
        $this->password = $password;
        return $this;
    }

    public function getPassword(): Password
    {
        return $this->password;
    }
}

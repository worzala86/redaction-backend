<?php

namespace App\Modules\User\Models;

use App\Model;
use App\Types\Mail;
use App\Types\Password;
use App\Types\UUID;

class UsersRegistrationsModel extends Model
{
    private $mail;
    private $password;
    private $code;
    private $uuid;

    public function setUuid(UUID $uuid)
    {
        $this->set('uuid', hex2bin($uuid));
        $this->uuid = $uuid;
        return $this;
    }

    public function getUuid(): UUID
    {
        return $this->uuid;
    }

    public function setCode(UUID $code)
    {
        $this->set('code', hex2bin($code));
        $this->code = $code;
        return $this;
    }

    public function getCode(): UUID
    {
        return $this->code;
    }

    public function setMail(Mail $mail)
    {
        $this->set('mail', $mail);
        $this->mail = $mail;
        return $this;
    }

    public function getMail(): Mail
    {
        return $this->mail;
    }

    public function setPassword(Password $password)
    {
        $this->set('password', hex2bin($password));
        $this->password = $password;
        return $this;
    }

    public function getPassword(): Password
    {
        return $this->password;
    }
}

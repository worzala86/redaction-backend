<?php

namespace App\Modules\User\Handlers;

use App\Database\DB;
use App\Handler;
use App\Modules\Projects\Models\ProjectsModel;
use App\Modules\User\Responses\UserStatusResponse;
use App\Request;

/**
 * Class UserStatusHandler
 * @package App\Modules\User\Handlers
 * @description Metoda służy do pobrania statusu użytkownika.
 */
class UserStatusHandler extends Handler
{
    public function __invoke(Request $request): UserStatusResponse
    {
        $user = $request->getUser();

        return (new UserStatusResponse)
            ->setLogged(($request->getCurrentUserId() > 0) ? true : false)
            ->setIsAdmin($user ? $user->isAdmin() : false);
    }
}

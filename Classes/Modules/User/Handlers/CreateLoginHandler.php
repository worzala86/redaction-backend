<?php

namespace App\Modules\User\Handlers;

use App\Exceptions\UserNotFoundException;
use App\Handler;
use App\Modules\Projects\Models\ProjectsModel;
use App\Modules\User\Exceptions\NotTheSamePasswordException;
use App\Modules\User\Models\UsersModel;
use App\Modules\User\Requests\CreateLoginRequest;
use App\Modules\User\Responses\CreateLoginResponse;

/**
 * Class CreateLoginHandler
 * @package App\Modules\User\Handlers
 * @description Metoda służy do logowania w serwisie
 */
class CreateLoginHandler extends Handler
{
    public function __invoke(CreateLoginRequest $request): CreateLoginResponse
    {
        $usersModel = (new UsersModel)
            ->where('`mail`=?', $request->getMail())
            ->where('`deleted`=?', 0)
            ->load();

        if (!$usersModel->isLoaded()) {
            throw new UserNotFoundException;
        }

        if ((string)$usersModel->getPassword() === (string)$request->getPassword()) {
            $_SESSION['user_id'] = $usersModel->getId();
        } else {
            throw new NotTheSamePasswordException;
        }

        $redirect = '/start';

        return (new CreateLoginResponse())
            ->setRedirect($redirect);
    }
}

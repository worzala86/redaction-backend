<?php

namespace App\Modules\User\Handlers;

use App\Handler;
use App\Modules\User\Exceptions\AccountExistsException;
use App\Modules\User\Exceptions\RegistrationCodeNotFoundException;
use App\Modules\User\Models\UsersModel;
use App\Modules\User\Models\UsersRegistrationsModel;
use App\Modules\User\Requests\ActivateAccountRequest;
use App\Modules\User\Responses\ActivateAccountResponse;
use App\Types\UUID;

/**
 * Class ActivateAccountHandler
 * @package App\Modules\User\Handlers
 * @description Metoda służy do aktywacji nowo dodanego użytkownika
 */
class ActivateAccountHandler extends Handler
{
    public function __invoke(ActivateAccountRequest $request): ActivateAccountResponse
    {
        $usersRegistrationsModel = (new UsersRegistrationsModel)
            ->where('`code`=?', hex2bin($request->getCode()))
            ->load();

        if (!$usersRegistrationsModel->isLoaded()) {
            throw new RegistrationCodeNotFoundException;
        }

        $usersModel = (new UsersModel)
            ->where('`mail`=?', (string)$usersRegistrationsModel->getMail())
            ->load();

        if ($usersModel->isLoaded()) {
            throw new AccountExistsException;
        }

        $usersId = (new UsersModel)
            ->setUuid(UUID::fake())
            ->setMail($usersRegistrationsModel->getMail())
            ->setPassword($usersRegistrationsModel->getPassword())
            ->insert();

        return (new ActivateAccountResponse)
            ->setSuccess($usersId ? true : false);
    }
}

<?php

namespace App\Modules\User\Handlers;

use App\Exceptions\UserNotFoundException;
use App\Handler;
use App\MailSender;
use App\Modules\User\Models\UsersModel;
use App\Modules\User\Requests\ResetPasswordRequest;
use App\Modules\User\Responses\ResetPasswordResponse;
use App\Types\UUID;

/**
 * Class ResetPasswordHandler
 * @package App\Modules\User\Handlers
 * @description Metoda służy do resetowania hasła w użytkownika według maila
 */
class ResetPasswordHandler extends Handler
{
    public function __invoke(ResetPasswordRequest $request): ResetPasswordResponse
    {
        $code = UUID::fake();

        $user = (new UsersModel)
            ->where('`mail`=?', $request->getMail())
            ->load();

        $mailSended = null;
        if (!$user->isLoaded()) {
            throw new UserNotFoundException;
        }
        $user->setReset($code)
            ->update();

        $mailHTML = file_get_contents(ROOT_DIRECTORY . '/Mails/ResetPasswordMail.html');
        $mailHTML = str_replace('{{link}}', DOMAIN . '/nowe-haslo/' . $code, $mailHTML);
        $mailHTML = str_replace('{{domain}}', DOMAIN, $mailHTML);

        $mailSended = (new MailSender)
            ->addAddress($request->getMail())
            ->addSubject('Reset hasła')
            ->addBody($mailHTML)
            ->send();

        return (new ResetPasswordResponse)
            ->setSuccess(($user->isLoaded() && $mailSended) ? true : false);
    }
}

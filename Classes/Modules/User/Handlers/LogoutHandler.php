<?php

namespace App\Modules\User\Handlers;

use App\Database\DB;
use App\Handler;
use App\Modules\User\Requests\LogoutRequest;
use App\Modules\User\Responses\LogoutResponse;
use App\Types\SESSID;

/**
 * Class LogoutHandler
 * @package App\Modules\User\Handlers
 * @description Metoda służy do wylogowania z serwisu
 */
class LogoutHandler extends Handler
{
    public function __invoke(LogoutRequest $request): LogoutResponse
    {
        DB::get()->execute('update `sessions` set deleted=? where sessid=?', [time(), hex2bin(session_id())]);

        $_COOKIE[session_name()] = null;
        setcookie(session_name(), null, -1, '/');
        $_SESSION['user_id'] = null;
        //session_id(SESSID::fake());

        return (new LogoutResponse)
            ->setSuccess(true);
    }
}

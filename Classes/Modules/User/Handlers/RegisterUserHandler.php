<?php

namespace App\Modules\User\Handlers;

use App\Handler;
use App\MailSender;
use App\Modules\User\Exceptions\AccountExistsException;
use App\Modules\User\Models\UsersModel;
use App\Modules\User\Models\UsersRegistrationsModel;
use App\Modules\User\Requests\RegisterUserRequest;
use App\Modules\User\Responses\RegisterUserResponse;
use App\Types\UUID;

/**
 * Class RegisterUserHandler
 * @package App\Modules\User\Handlers
 * @description Metoda służy do rejestracji nowego użytkownika w serwisie
 */
class RegisterUserHandler extends Handler
{
    public function __invoke(RegisterUserRequest $request): RegisterUserResponse
    {
        $usersModel = (new UsersModel)
            ->where('`mail`=?', (string)$request->getMail())
            ->load();

        if ($usersModel->isLoaded()) {
            throw new AccountExistsException;
        }

        $code = UUID::fake();

        $usersRegistrationsId = (new UsersRegistrationsModel)
            ->setUuid(UUID::fake())
            ->setMail($request->getMail())
            ->setPassword($request->getPassword())
            ->setCode($code)
            ->insert();

        $mailHTML = file_get_contents(ROOT_DIRECTORY . '/Mails/RegistrationMail.html');
        $mailHTML = str_replace('{{link}}', DOMAIN . '/aktywacja/' . $code, $mailHTML);
        $mailHTML = str_replace('{{domain}}', DOMAIN, $mailHTML);

        $mailSended = (new MailSender)
            ->addAddress($request->getMail())
            ->addSubject('Rejestracja')
            ->addBody($mailHTML)
            ->send();

        return (new RegisterUserResponse)
            ->setSuccess(($usersRegistrationsId && $mailSended) ? true : false);
    }
}

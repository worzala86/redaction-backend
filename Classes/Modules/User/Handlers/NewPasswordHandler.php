<?php

namespace App\Modules\User\Handlers;

use App\Handler;
use App\Modules\User\Exceptions\WrondPasswordResetCodeException;
use App\Modules\User\Models\UsersModel;
use App\Modules\User\Requests\NewPasswordRequest;
use App\Modules\User\Responses\NewPasswordResponse;

/**
 * Class NewPasswordHandler
 * @package App\Modules\User\Handlers
 * @description Metoda służy do zapisu nowego hasła w serwisie
 */

class NewPasswordHandler extends Handler
{
    public function __invoke(NewPasswordRequest $request): NewPasswordResponse
    {
        $user = (new UsersModel)
            ->where('`reset`=?', hex2bin($request->getCode()))
            ->load();

        if(!$user->isLoaded()){
            throw new WrondPasswordResetCodeException;
        }

        $user->setPassword($request->getPassword())
            ->update();

        return (new NewPasswordResponse)
            ->setSuccess(true);
    }
}

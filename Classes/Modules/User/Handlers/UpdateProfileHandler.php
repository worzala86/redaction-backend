<?php

namespace App\Modules\User\Handlers;

use App\Handler;
use App\Modules\User\Models\UsersModel;
use App\Modules\User\Requests\GetProfileRequest;
use App\Modules\User\Requests\UpdateProfileRequest;
use App\Modules\User\Responses\GetProfileResponse;
use App\Modules\User\Responses\UpdateProfileResponse;

/**
 * Class UpdateProfileHandler
 * @package App\Modules\User\Handlers
 * @description Metoda służy do aktualizacji danych użytkownika.
 */
class UpdateProfileHandler extends Handler
{
    public function __invoke(UpdateProfileRequest $request): UpdateProfileResponse
    {
        $updated = (new UsersModel)
            ->setFirstName($request->getFirstName())
            ->setLastName($request->getLastName())
            ->update($request->getCurrentUserId());

        return (new UpdateProfileResponse)
            ->setSuccess($updated);
    }
}
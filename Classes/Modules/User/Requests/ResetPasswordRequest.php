<?php

namespace App\Modules\User\Requests;

use App\Request;
use App\Types\Mail;

class ResetPasswordRequest extends Request
{
    private $mail;

    /**
     * @param Mail $mail
     * @description Adres mailowy użytkownika, na ten adres będzie wysłany link do zresetowania hasła
     * @return $this
     */
    public function setMail(Mail $mail = null)
    {
        $this->mail = $mail;
        return $this;
    }

    public function getMail(): Mail
    {
        return $this->mail;
    }
}
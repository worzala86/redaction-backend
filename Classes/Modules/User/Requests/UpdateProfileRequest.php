<?php

namespace App\Modules\User\Requests;

use App\UserRequest;

class UpdateProfileRequest extends UserRequest
{
    private $firstName;
    private $lastName;

    /**
     * @param string $firstName
     * @description Imię użytkownika
     * @return $this
     */
    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $lastName
     * @description Nazwisko użytkownika
     * @return $this
     */
    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }
}
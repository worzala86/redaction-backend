<?php

namespace App\Modules\User\Exceptions;

use App\NamedException;

class AccountExistsException extends \Exception implements NamedException
{
}

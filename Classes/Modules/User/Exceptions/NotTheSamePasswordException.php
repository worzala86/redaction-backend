<?php

namespace App\Modules\User\Exceptions;

use App\NamedException;

class NotTheSamePasswordException extends \Exception implements NamedException
{
}

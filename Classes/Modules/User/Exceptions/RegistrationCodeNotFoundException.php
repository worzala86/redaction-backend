<?php

namespace App\Modules\User\Exceptions;

use App\NamedException;

class RegistrationCodeNotFoundException extends \Exception implements NamedException
{
}

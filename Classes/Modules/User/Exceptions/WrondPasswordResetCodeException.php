<?php

namespace App\Modules\User\Exceptions;

use App\NamedException;

class WrondPasswordResetCodeException extends \Exception implements NamedException
{
}

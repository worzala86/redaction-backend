<?php

namespace App\Modules\User\Responses;

use App\Response;

class UpdatePasswordResponse extends Response
{
    private $success;

    /**
     * @param bool $success
     * @description Wartość informuje o tym czy pomyślnie zapisano nowe hasło
     * @return $this
     */
    public function setSuccess(bool $success){
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool {
        return $this->success;
    }
}
<?php

namespace App\Modules\User\Responses;

use App\Response;

class ActivateAccountResponse extends Response
{
    private $success;

    /**
     * @param bool $success
     * @description Wartość informuje o tym czy aktywacja przebiegła pomyślnie
     * @return $this
     */
    public function setSuccess(bool $success){
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool {
        return $this->success;
    }
}

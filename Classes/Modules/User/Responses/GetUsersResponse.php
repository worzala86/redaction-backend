<?php

namespace App\Modules\User\Responses;

use App\Containers\UsersContainer;
use App\Response;

class GetUsersResponse extends Response
{
    private $users;

    /**
     * @param UsersContainer $users
     * @description Pele zawiera listę użytkowników
     * @return $this
     */
    public function setUsers(UsersContainer $users)
    {
        $this->users = $users;
        return $this;
    }

    public function getUsers(): UsersContainer
    {
        return $this->users;
    }
}
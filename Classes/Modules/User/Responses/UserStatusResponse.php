<?php

namespace App\Modules\User\Responses;

use App\Response;
use App\Types\UUID;

class UserStatusResponse extends Response
{
    private $logged;
    private $isAdmin;

    /**
     * @param bool $logged
     * @description Wartość informuje o tym czy użytkownik w sessjii jest zalogowany
     * @return $this
     */
    public function setLogged(bool $logged)
    {
        $this->logged = $logged;
        return $this;
    }

    public function getLogged(): bool
    {
        return $this->logged;
    }

    /**
     * @param bool $isAdmin
     * @description Wartość informuje o tym czy użytkownik jest administratorem
     * @return $this
     */
    public function setIsAdmin(bool $isAdmin)
    {
        $this->isAdmin = $isAdmin;
        return $this;
    }

    public function getIsAdmin(): bool
    {
        return $this->isAdmin;
    }
}

<?php

namespace App;

use App\Modules\Doc\Handlers;

return [
    ['method'=>'get', 'url'=>'api/documentation', 'handler'=>Handlers\GetDocHandler::class],
];
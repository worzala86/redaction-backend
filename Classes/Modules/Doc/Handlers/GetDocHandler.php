<?php

namespace App\Modules\Doc\Handlers;

use App\CollectionTrait;
use App\Containers\HtmlElementsContainer;
use App\DocComment;
use App\Handler;
use App\Modules\Doc\Responses\GetDocResponse;
use App\Request;

/**
 * Class GetDocHandler
 * @package App\Modules\Doc\Handlers
 * @description Test test
 */

class GetDocHandler extends Handler
{
    private $types;

    public function __construct()
    {
        $this->types = [];
    }

    public function __invoke(Request $request): GetDocResponse
    {
        $modules = [];
        foreach (glob(substr(__DIR__, 0, strpos(__DIR__, '/Modules/Doc/Handlers')) . '/Modules/*/Route.php') as $routeDir) {
            preg_match('/Modules\/([a-zA-Z0-9]+)*\/Route.php/', $routeDir, $matches);
            $module = $matches[1];
            if($module=='Doc'){
                continue;
            }
            $routesFromDir = require($routeDir);
            foreach ($routesFromDir as $route) {
                if(isset($route['regex'])) {
                    unset($route['regex']);
                }
                $modules[$module][] = $route;

                preg_match('/Handlers\\\\([a-zA-Z0-9]+)*Handler$/', $route['handler'], $matches);
                $name = $matches[1];
                $modules[$module][count($modules[$module])-1]['name'] = $name;

                $handlerReflection = new \ReflectionClass($route['handler']);
                $handlerInvoke = $handlerReflection->getMethod('__invoke');
                $docComment = $handlerReflection->getDocComment();
                unset($handlerReflection);

                $doc = new DocComment($docComment);
                $classDescription = $doc->getByTag('description');
                $modules[$module][count($modules[$module])-1]['description'] = $classDescription;
                unset($doc);

                $requestParameters = $handlerInvoke->getParameters();
                $requestParameter = $requestParameters[0];
                $className = $requestParameter->getClass()->getName();
                $modules[$module][count($modules[$module])-1]['request'] = $className;
                $properties = [];
                $this->getProperties($className, $properties, false);
                $this->insertType($className, $properties);

                $returnType = $handlerInvoke->getReturnType();
                $className = $returnType->getName();
                $modules[$module][count($modules[$module])-1]['response'] = $className;
                $properties = [];
                $this->getProperties($className, $properties, false);
                $this->insertType($className, $properties);

                if(isset($modules[$module][count($modules[$module])-1]['handler'])) {
                    unset($modules[$module][count($modules[$module])-1]['handler']);
                }
            }
        }

        return (new GetDocResponse)
            ->setModules($modules)
            ->setTypes($this->types);
    }

    private function insertType($className, $properties){
        $this->types[$className] = $properties;
    }

    private function getProperties($className, &$container, $deepSearch = true){
        $reflectionClass = new \ReflectionClass($className);
        $methods = $reflectionClass->getMethods();
        foreach ($methods as $method) {
            $name = $method->getName();
            if ((strpos($name, 'set') === 0)&&$reflectionClass->hasMethod('get'.substr($name, 3))) {
                $propertyName = lcfirst(substr($name, 3));
                $parameter = $method->getParameters()[0];
                $type = (string)$parameter->getType();
                if($type==HtmlElementsContainer::class){
                    continue;
                }
                $doc = $method->getDocComment();
                $docComment = new DocComment($doc);
                $description = $docComment->getByTag('description');
                unset($doc);
                $container[] = [
                    'name'=>$propertyName,
                    'type'=>$type,
                    'description'=>$description,
                ];
                if(!empty($type)&&!(($type=='int')||($type=='string')||($type=='array')||($type=='float')||($type=='bool'))){
                    $reflectionClassType = new \ReflectionClass($type);
                    $traits = $reflectionClassType->getTraits();
                    foreach ($traits as $key=>$trait){
                        $traits[$key] = $trait->getName();
                    }
                    if(in_array(CollectionTrait::class, $traits)){
                        $reflectionClassType = $reflectionClassType->getParentClass();
                        $container[count($container) - 1]['array'] = true;
                    }
                    if($reflectionClassType->getParentClass()->getName()=='App\\Container'){
                        $container[count($container) - 1]['container'] = true;
                        $properties = [];
                        $this->getProperties($type, $properties);
                        if($properties&&$deepSearch) {
                            $container[count($container) - 1]['properties'] = $properties;
                        }
                        $this->insertType($type, $properties);
                    }
                    unset($reflectionClassType);
                }
            }
        }
        unset($methods);
        unset($reflectionClass);
    }
}
<?php

namespace App\Modules\Doc\Responses;

use App\Containers\TestContainer;
use App\Response;

class GetDocResponse extends Response
{
    private $modules;
    private $types;

    /**
     * @param array $modules
     * @description Lista modułów
     * @return $this
     */
    public function setModules(array $modules){
        $this->modules = $modules;
        return $this;
    }

    public function getModules(){
        return $this->modules;
    }

    /**
     * @param array $types
     * @description Lista typów
     * @return $this
     */
    public function setTypes(array $types){
        $this->types = $types;
        return $this;
    }

    public function getTypes(){
        return $this->types;
    }
}
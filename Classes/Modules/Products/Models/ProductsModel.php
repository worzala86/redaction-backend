<?php

namespace App\Modules\Products\Models;

use App\Model;
use App\Types\UUID;

class ProductsModel extends Model
{
    private $id;
    private $uuid;
    private $sku;
    private $name;

    public function setName(string $name)
    {
        $this->set('name', $name);
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setSku(string $sku)
    {
        $this->set('sku', $sku);
        $this->sku = $sku;
        return $this;
    }

    public function getSku(): string
    {
        return $this->sku;
    }

    public function setUuid(UUID $uuid)
    {
        $this->set('uuid', hex2bin($uuid));
        $this->uuid = $uuid;
        return $this;
    }

    public function getUuid(): UUID
    {
        return $this->uuid;
    }

    public function setId(int $id)
    {
        $this->set('id', $id);
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }
}

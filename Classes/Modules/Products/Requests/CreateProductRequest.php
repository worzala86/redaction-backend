<?php

namespace App\Modules\Products\Requests;

use App\UserRequest;

class CreateProductRequest extends UserRequest
{
    private $sku;
    private $name;

    /**
     * @param string $name
     * @description Nazwa produktu
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $sku
     * @description SKU produktu
     * @return $this
     */
    public function setSku(string $sku)
    {
        $this->sku = $sku;
        return $this;
    }

    public function getSku(): string
    {
        return $this->sku;
    }
}

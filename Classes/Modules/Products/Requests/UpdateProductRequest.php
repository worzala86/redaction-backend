<?php

namespace App\Modules\Products\Requests;

use App\Types\UUID;
use App\UserRequest;

class UpdateProductRequest extends UserRequest
{
    private $id;
    private $sku;
    private $name;

    /**
     * @param UUID $id
     * @description Identyfikator produktu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @description Nazwa produktu
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $sku
     * @description SKU produktu
     * @return $this
     */
    public function setSku(string $sku)
    {
        $this->sku = $sku;
        return $this;
    }

    public function getSku(): string
    {
        return $this->sku;
    }
}

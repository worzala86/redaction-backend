<?php

namespace App\Modules\Products\Requests;

use App\Types\UUID;
use App\UserRequest;

class GetProductRequest extends UserRequest
{
    private $id;

    /**
     * @param UUID $id
     * @description Identyfikator produktu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }
}

<?php

namespace App\Modules\Products\Handlers;

use App\Handler;
use App\Modules\Products\Models\ProductsModel;
use App\Modules\Products\Requests\CreateProductRequest;
use App\Modules\Products\Responses\CreateProductResponse;
use App\Types\UUID;

/**
 * Class CreateProducttHandler
 * @package App\Modules\Products\Handlers
 * @description Metoda służy do utworzenia nowego produktu
 */

class CreateProductHandler extends Handler
{
    public function __invoke(CreateProductRequest $request): CreateProductResponse
    {
        $uuid = UUID::fake();

        (new ProductsModel)
            ->setUuid($uuid)
            ->setSku($request->getSku())
            ->setName($request->getName())
            ->insert();

        return (new CreateProductResponse)
            ->setId($uuid);
    }
}

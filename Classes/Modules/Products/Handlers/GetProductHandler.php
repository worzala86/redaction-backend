<?php

namespace App\Modules\Products\Handlers;

use App\Handler;
use App\Modules\Products\Models\ProductsModel;
use App\Modules\Products\Requests\GetProductRequest;
use App\Modules\Products\Responses\GetProductResponse;
use App\Modules\Products\Exceptions\ProductNotFoundException;

/**
 * Class GetProductHandler
 * @package App\Modules\Products\Handlers
 * @description Metoda służy do pobrania szczegułów produktów
 */

class GetProductHandler extends Handler
{
    public function __invoke(GetProductRequest $request): GetProductResponse
    {
        $productModel = (new ProductsModel)
            ->load($request->getId(), true);

        if(!$productModel->isLoaded()){
            throw new ProductNotFoundException;
        }

        return (new GetProductResponse)
            ->setSku($productModel->getSku())
            ->setName($productModel->getName());
    }
}

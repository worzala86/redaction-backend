<?php

namespace App\Modules\Products\Handlers;

use App\Containers\ProductContainer;
use App\Containers\ProductsContainer;
use App\Handler;
use App\Modules\Products\Requests\GetProductsRequest;
use App\Modules\Products\Responses\GetProductsResponse;
use App\Modules\Products\Collections\ProductsCollection;

/**
 * Class GetProductsHandler
 * @package App\Modules\Products\Handlers
 * @description Metoda służy do pobrania listy produktów
 */

class GetProductsHandler extends Handler
{
    public function __invoke(GetProductsRequest $request): GetProductsResponse
    {
        $productsCollection = (new ProductsCollection)
            ->loadAll();

        $products = new ProductsContainer;
        foreach ($productsCollection as $productsModel){
            $products->add(
                (new ProductContainer)
                    ->setId($productsModel->getUuid())
                    ->setSku($productsModel->getSku())
                    ->setName($productsModel->getName())
            );
        }

        return (new GetProductsResponse)
            ->setProducts($products);
    }
}

<?php

namespace App\Modules\Products\Handlers;

use App\Handler;
use App\Modules\Products\Exceptions\ProductNotFoundException;
use App\Modules\Products\Models\ProductsModel;
use App\Modules\Products\Requests\UpdateProductRequest;
use App\Modules\Products\Responses\UpdateProductResponse;

/**
 * Class UpdateProductHandler
 * @package App\Modules\Products\Handlers
 * @description Metoda służy do aktualizacji produktu
 */
class UpdateProductHandler extends Handler
{
    public function __invoke(UpdateProductRequest $request): UpdateProductResponse
    {
        $productModel = (new ProductsModel)
            ->load($request->getId(), true);

        if (!$productModel->isLoaded()) {
            throw new ProductNotFoundException;
        }

        $productModel
            ->setSku($request->getSku())
            ->setName($request->getName())
            ->update();

        return (new UpdateProductResponse)
            ->setSuccess(true);
    }
}

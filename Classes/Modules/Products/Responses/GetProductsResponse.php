<?php

namespace App\Modules\Products\Responses;

use App\Containers\ProductsContainer;
use App\Response;

class GetProductsResponse extends Response
{
    private $products;

    /**
     * @param ProductsContainer $products
     * @description Lista produktów
     * @return $this
     */
    public function setProducts(ProductsContainer $products)
    {
        $this->products = $products;
        return $this;
    }

    public function getProducts(): ProductsContainer
    {
        return $this->products;
    }
}

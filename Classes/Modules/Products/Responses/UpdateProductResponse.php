<?php

namespace App\Modules\Products\Responses;

use App\Response;
use App\Types\UUID;

class UpdateProductResponse extends Response
{
    private $success;

    /**
     * @param bool $success
     * @description Informacja o powodzeniu zapisu
     * @return $this
     */
    public function setSuccess(bool $success)
    {
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool
    {
        return $this->success;
    }
}

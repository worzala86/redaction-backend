<?php

namespace App\Modules\Products\Responses;

use App\Response;
use App\Types\UUID;

class CreateProductResponse extends Response
{
    private $id;

    /**
     * @param UUID $id
     * @description Pole zawiera identyfikator utworzonego produktu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }
}

<?php

namespace App;

use App\Modules\Products\Handlers;
use App\Types\UUID;

return [
    ['method'=>'post', 'url'=>'api/products', 'handler'=>Handlers\CreateProductHandler::class],
    ['method'=>'get', 'url'=>'api/products', 'handler'=>Handlers\GetProductsHandler::class],
    ['method'=>'get', 'url'=>'api/products/:id', 'handler'=>Handlers\GetProductHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'put', 'url'=>'api/products/:id', 'handler'=>Handlers\UpdateProductHandler::class, 'regex'=>['id'=>UUID::$pattern]],
];

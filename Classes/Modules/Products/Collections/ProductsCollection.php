<?php

namespace App\Modules\Products\Collections;

use App\CollectionTrait;
use App\Modules\Products\Models\ProductsModel;
use App\PaginationTrait;

class ProductsCollection extends ProductsModel implements \Iterator
{
    use CollectionTrait;
    use PaginationTrait;
}

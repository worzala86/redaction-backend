<?php

namespace App\Modules\Products\Exceptions;

use App\NamedException;

class ProductNotFoundException extends \Exception implements NamedException
{
}

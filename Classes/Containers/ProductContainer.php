<?php

namespace App\Containers;

use App\Container;
use App\Types\UUID;

class ProductContainer extends Container
{
    private $id;
    private $sku;
    private $name;

    /**
     * @param int $id
     * @description Identyfikator produktu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param string $sku
     * @description SKU produktu
     * @return $this
     */
    public function setSku(string $sku)
    {
        $this->sku = $sku;
        return $this;
    }

    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @param string $name
     * @description Nazwa produktu
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }
}

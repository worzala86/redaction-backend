<?php

namespace App\Containers;

use App\CollectionTrait;

class ProductsContainer extends ProductContainer implements \Iterator
{
    use CollectionTrait;
}

<?php

namespace App\Types;

use App\Exceptions\IncorrectMailAddressException;
use App\Exceptions\WrongTimeException;
use App\Type;

class Time extends Type
{
    public function __construct($data = null)
    {
        $time = (int)$data;
        if ($time < 1547502942) {
            throw new WrongTimeException;
        }
        parent::__construct($data);
    }

    public static function fake()
    {
        return new Time(time());
    }

    public function getFlatData()
    {
        return (int)$this->data;
    }
}
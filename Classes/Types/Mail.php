<?php

namespace App\Types;

use App\Exceptions\IncorrectMailAddressException;
use App\Type;

class Mail extends Type
{
    static $pattern = '([a-zA-Z0-9.]+@[a-zA-Z0-9.]+.[a-zA-Z0-9.]+)';

    public function __construct($data = null)
    {
        preg_match('/^'.self::$pattern.'$/', $data, $matches);
        if(empty($matches)){
            throw new IncorrectMailAddressException;
        }
        parent::__construct($data);
    }
}
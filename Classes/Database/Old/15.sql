create table pages_elements_fields(
    field binary(4) not null,
    value varchar(50) not null,
    page_element_id int unsigned not null,
    constraint `fk_pages_elements_fields_page_element_id` foreign key (page_element_id) references pages_elements(id)
        on update cascade on delete no action,
    primary key(field,page_element_id)
)
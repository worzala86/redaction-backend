ALTER TABLE `redaction`.`pages_datasets` DROP PRIMARY KEY,
    ADD PRIMARY KEY (`page_id`, `key`, `element_position_id`) USING BTREE;
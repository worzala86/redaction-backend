CREATE TABLE `projects_elements`
(
    `id`         int(10) unsigned NOT NULL AUTO_INCREMENT,
    `element_id` int(10) unsigned NOT NULL,
    `project_id` int(10) unsigned NOT NULL,
    `added`      int(10) unsigned NOT NULL,
    `uuid`       binary(8)        NOT NULL,
    `selector`   varchar(50) DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uuid` (`uuid`),
    KEY `fk_projects_elements_element_id` (`element_id`),
    KEY `fk_projects_elements_project_id` (`project_id`),
    CONSTRAINT `fk_projects_elements_element_id` FOREIGN KEY (`element_id`) REFERENCES `elements` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
    CONSTRAINT `fk_projects_elements_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
);
CREATE TABLE `projects_elements_fields`
(
    `field`              binary(4)        NOT NULL,
    `value`              varchar(50)      NOT NULL,
    `project_element_id` int(10) unsigned NOT NULL,
    PRIMARY KEY (`field`, `project_element_id`),
    KEY `fk_pages_elements_fields_project_element_id` (`project_element_id`),
    CONSTRAINT `fk_pages_elements_fields_page_project_element_id` FOREIGN KEY (`project_element_id`) REFERENCES `projects_elements` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
);
CREATE TABLE `projects_datasets`
(
    `project_id`          int(10) unsigned NOT NULL,
    `key`                 binary(4)        NOT NULL,
    `value`               text,
    `element_position_id` int(10) unsigned NOT NULL,
    PRIMARY KEY (`project_id`, `key`, `element_position_id`) USING BTREE,
    KEY `fk_projects_datasets_element_position_id` (`element_position_id`),
    CONSTRAINT `fk_projects_datasets_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
);
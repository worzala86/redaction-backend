create or replace view images_view as select id, uuid, user_id, file_id, pixabay_id, deleted,
    (select count(*) from tags_images where image_id=images.id) as `tags_count` from images;
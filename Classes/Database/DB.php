<?php

namespace App\Database;

class DB
{
    static $connection;

    static public function get(): Database
    {
        if (!isset(self::$connection)) {
            self::$connection = new Database(
                DATABASE_CONNECTION['HOST'],
                DATABASE_CONNECTION['DATABASE'],
                DATABASE_CONNECTION['USER'],
                DATABASE_CONNECTION['PASSWORD']);
        }
        return self::$connection;
    }
}

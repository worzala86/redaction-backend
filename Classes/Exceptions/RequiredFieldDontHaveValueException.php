<?php

namespace App\Exceptions;

use App\NamedException;

class RequiredFieldDontHaveValueException extends \Exception implements NamedException
{
}
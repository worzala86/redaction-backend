<?php

namespace App\Exceptions;

use App\NamedException;

class UserNotFoundException extends \Exception implements NamedException
{
}
<?php

namespace App\Exceptions;

use App\NamedException;

class IncorrectDatasetFieldNameException extends \Exception implements NamedException
{
}
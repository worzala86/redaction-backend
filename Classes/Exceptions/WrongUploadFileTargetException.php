<?php

namespace App\Exceptions;

use App\NamedException;

class WrongUploadFileTargetException extends \Exception implements NamedException
{
}
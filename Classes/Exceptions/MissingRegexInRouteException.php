<?php

namespace App\Exceptions;

use App\NamedException;

class MissingRegexInRouteException extends \Exception implements NamedException
{
}
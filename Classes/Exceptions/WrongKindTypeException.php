<?php

namespace App\Exceptions;

use App\NamedException;

class WrongKindTypeException extends \Exception implements NamedException
{
}
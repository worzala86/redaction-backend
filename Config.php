<?php

define('DEBUG', true);

error_reporting(E_ALL);
ini_set('display_errors', true);

define('SESSION_TIMEOUT', 60 * 60);

define('ROOT_DIRECTORY', __DIR__);
define('FILES_DIRECTORY', __DIR__ . '/Files');

define('DATABASE_CONNECTION', [
    'HOST' => 'localhost',
    'DATABASE' => 'redaction3',
    'USER' => 'root',
    'PASSWORD' => 'root',
]);

define('DOMAIN', 'http://localhost:4200');

define('MAIL_AUTHORIZATION', [
    'HOST' => 'smtp.gmail.com',
    'SMTPAUTH' => true,
    'USER' => 'megazin.automat@gmail.com',
    'PASSWORD' => 'M2fgjzXDFmykaNg',
    'SMTPSECURE' => 'tsl',
    'PORT' => 587,
    'SENT_FROM' => 'megazin.automat@gmail.com',
    'REPLY_TO' => 'megazin.automat@gmail.com',
    'YOUR_NAME' => 'RedAction',
]);

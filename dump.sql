-- MySQL dump 10.16  Distrib 10.1.40-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: redaction3
-- ------------------------------------------------------
-- Server version	10.1.40-MariaDB-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` binary(16) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `thumb_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `fk_files_added_by` (`added_by`),
  KEY `fk_files_deleted_by` (`deleted_by`),
  KEY `fk_files_added_ip_id` (`added_ip_id`),
  KEY `fk_files_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_files_user_id` (`user_id`),
  KEY `fk_files_thumb_id` (`thumb_id`),
  CONSTRAINT `fk_files_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ips` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ips` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_thumb_id` FOREIGN KEY (`thumb_id`) REFERENCES `files` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ips`
--

DROP TABLE IF EXISTS `ips`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ips` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `date` int(10) unsigned NOT NULL,
  `session_id` int(10) unsigned DEFAULT NULL,
  `host_name` varchar(250) DEFAULT NULL COMMENT 'Nazwa hosta z którego łączy się użytkownik',
  `country_name` varchar(250) DEFAULT NULL,
  `region_name` varchar(250) DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `zip_code` varchar(50) DEFAULT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ip` (`ip`,`user_id`,`session_id`),
  KEY `fk_ip_user_id` (`user_id`),
  KEY `fk_ip_session_id` (`session_id`),
  CONSTRAINT `fk_ip_session_id` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_ip_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ips`
--

LOCK TABLES `ips` WRITE;
/*!40000 ALTER TABLE `ips` DISABLE KEYS */;
INSERT INTO `ips` VALUES (1,1,NULL,1561656121,NULL,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(2,1,NULL,1561656170,1,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(3,1,NULL,1561657390,2,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(4,1,NULL,1561663236,3,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(5,1,NULL,1561663295,4,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(6,1,NULL,1561663310,5,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(7,1,NULL,1561666268,6,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(8,1,NULL,1561667700,7,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(9,1,NULL,1561667705,9,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(10,1,NULL,1561667835,8,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(11,1,NULL,1561836558,10,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(12,1,NULL,1561837452,11,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(13,1,NULL,1561842765,12,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(14,1,NULL,1561842813,13,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `ips` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` binary(16) DEFAULT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `sku` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `fk_products_added_by` (`added_by`),
  KEY `fk_products_deleted_by` (`deleted_by`),
  KEY `fk_products_updated_by` (`updated_by`),
  KEY `fk_products_added_ip_id` (`added_ip_id`),
  KEY `fk_products_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_products_updated_ip_id` (`updated_ip_id`),
  CONSTRAINT `fk_products_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_products_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ips` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_products_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_products_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ips` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_products_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_products_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ips` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'b�0��XE4�����Ք',1561839592,2,13,NULL,NULL,NULL,0,NULL,NULL,'testowe SKU','nazwa produkty - testowa'),(2,'u\0X2/����8�Dj�\Z',1561842418,2,13,NULL,NULL,NULL,0,NULL,NULL,'sku 22','produkt testowy');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `sessid` binary(32) NOT NULL,
  `access` int(10) unsigned DEFAULT NULL,
  `data` text,
  `user_id` int(10) unsigned DEFAULT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sessid` (`sessid`),
  KEY `fk_session_user_id` (`user_id`),
  CONSTRAINT `fk_session_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES ('�CM���w(�����|L�=����Y��-)/�\0',1561656155,'ip_id|s:1:\"2\";user_id|i:2;',2,1561656170,1),('�c��YՁR��i��Z�h8�UֈI�-Cs4K�',1561657385,'ip_id|s:1:\"3\";user_id|i:2;',2,1561657390,2),('GS�S�&�/`_u�<�V��0�#H�\\Z߉(\"p',1561663183,'ip_id|s:1:\"4\";user_id|i:2;',2,1561663236,3),('�I_)�G{E]�)���Ƙs��B	-�5�7X$�9{',1561663291,'ip_id|s:1:\"5\";user_id|i:2;',2,1561663295,4),('[ȕ�!�=��g�8ӿ���q�	��x���j�6�\0',1561663307,'ip_id|s:1:\"6\";user_id|i:2;',2,1561663310,5),('��%i(�jK����5y�\r\\9XX�۹BjV�s',1561665416,'ip_id|s:1:\"7\";user_id|i:2;',2,1561666268,6),('ʼl�oԦ����JA�X��%ڡp�\nd�cM�',1561667696,'ip_id|s:1:\"8\";user_id|i:2;',2,1561667700,7),('x2�O\nF#zF_�g0Q�\r\Z&�6��yF��<',1561667824,'ip_id|s:2:\"10\";user_id|i:2;',2,1561667836,8),('[�,̪gcK���\n@��G��JW��C��Ŝ�;',1561667706,'ip_id|s:1:\"9\";user_id|i:2;',2,0,9),('[�� qk\'��=f��o����u6Z��A�=��',1561836550,'ip_id|s:2:\"11\";user_id|i:2;',2,1561836558,10),('���!�%�<_ԣq��M*�]e�Y4�Q�G�g�',1561837444,'ip_id|s:2:\"12\";user_id|i:2;',2,1561837452,11),('��R�;���*�	Y�&I��&=\\ȱ��W%v',1561842647,'ip_id|s:2:\"13\";user_id|i:2;',2,1561842766,12),('IY7KtAJ�*�}4��:�\Z�ݒV��_W\0Lϫl�',1561842813,'ip_id|s:2:\"14\";user_id|i:2;',2,0,13);
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mail` varchar(90) DEFAULT NULL,
  `uuid` binary(16) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned DEFAULT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `password` binary(64) NOT NULL,
  `reset` binary(16) DEFAULT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  UNIQUE KEY `email_2` (`mail`,`deleted`),
  KEY `added_by` (`added_by`),
  KEY `added_ip_id` (`added_ip_id`),
  KEY `deleted_by` (`deleted_by`),
  KEY `deleted_ip_id` (`deleted_ip_id`),
  KEY `updated_by` (`updated_by`),
  KEY `updated_ip_id` (`updated_ip_id`),
  KEY `reset` (`reset`),
  KEY `admin` (`admin`),
  CONSTRAINT `fk_users_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ips` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ips` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ips` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'worzala86@gmail.com','�|���5�	6����_',1561601147,NULL,1,0,NULL,NULL,1561662699,NULL,4,'3�a\Z5����b��ǿbZ��:AS�Y��޴C�tXM��E�(;\\{��-�G�f�u����#���','B�}T��R��D�\\�',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_registrations`
--

DROP TABLE IF EXISTS `users_registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_registrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` binary(16) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned DEFAULT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `mail` varchar(250) NOT NULL,
  `password` binary(64) NOT NULL,
  `code` binary(16) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `added_by` (`added_by`),
  KEY `added_ip_id` (`added_ip_id`),
  KEY `deleted_by` (`deleted_by`),
  KEY `deleted_ip_id` (`deleted_ip_id`),
  CONSTRAINT `fk_users_registrations_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_registrations_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ips` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_registrations_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_registrations_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ips` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_registrations`
--

LOCK TABLES `users_registrations` WRITE;
/*!40000 ALTER TABLE `users_registrations` DISABLE KEYS */;
INSERT INTO `users_registrations` VALUES (1,'��9�V1�p+�<R��',1561600919,NULL,1,0,NULL,NULL,'worzala86@gmail.com','3�a\Z5����b��ǿbZ��:AS�Y��޴C�tXM��E�(;\\{��-�G�f�u����#���','�R����R�y��C��');
/*!40000 ALTER TABLE `users_registrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `version`
--

DROP TABLE IF EXISTS `version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `version` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `number` smallint(5) unsigned NOT NULL,
  `date` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `version`
--

LOCK TABLES `version` WRITE;
/*!40000 ALTER TABLE `version` DISABLE KEYS */;
/*!40000 ALTER TABLE `version` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-29 23:13:58
